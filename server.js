const express = require('express')
const {
    Sequelize
} = require('sequelize')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')
const {
    readdirSync
} = require('fs')

// app

const app = express();

// db
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_TYPE
});

try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');

    // sequelize.authenticate()
    //     .then(() => console.log('Connection has been established successfully.'))
    //     .catch(() => console.log('unable to connect.'))
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

// middleware
app.use(morgan('dev'))
app.use(bodyParser.json({
    limit: "2mb"
}))
app.use(cors)

// routes autoloading
const prefix = "/api";
readdirSync("./routes").map((r) => app.use(prefix, require("./routes/" + r)))

// running apps
const port = process.env.APP_PORT || 3003
app.listen(port, () => console.log(`running on ${port}`))